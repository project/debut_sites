
; Drupal version
core = 6.x
api = 2

; Contrib modules
projects[ctools][subdir] = contrib
projects[ctools][version] = 1.8
projects[debut][subdir] = contrib
projects[debut_sites][subdir] = contrib
projects[features][subdir] = contrib
projects[features][version] = 1.0
projects[purl][subdir] = contrib
projects[purl][version] = 1.0-beta13
projects[spaces][subdir] = contrib
projects[spaces][version] = 3.0
projects[strongarm][subdir] = contrib
projects[strongarm][version] = 2.0

