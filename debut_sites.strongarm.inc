<?php

/**
 * Implementation of hook_strongarm().
 */
function debut_sites_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'purl_method_spaces_taxonomy';
  $strongarm->value = 'path';

  $export['purl_method_spaces_taxonomy'] = $strongarm;

  // Fetch the correct vocabulary ID.
  if ($vid = debut_sites_get_vid()) {
    $strongarm = new stdClass;
    $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
    $strongarm->api_version = 1;
    $strongarm->name = 'spaces_taxonomy_vid';
    $strongarm->value = $vid;

    $export['spaces_taxonomy_vid'] = $strongarm;
  }
  return $export;
}
