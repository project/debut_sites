<?php

/**
 * Implementation of hook_taxonomy_default_vocabularies().
 */
function debut_sites_taxonomy_default_vocabularies() {
  return array(
    'sites' => array(
      'name' => 'Sites',
      'description' => '',
      'help' => '',
      'relations' => '1',
      'hierarchy' => '1',
      'multiple' => '0',
      'required' => '0',
      'tags' => '0',
      'module' => 'features_sites',
      'weight' => '0',
      'nodes' => array(
        'story' => 'story',
        'page' => 'page',
      ),
    ),
  );
}
